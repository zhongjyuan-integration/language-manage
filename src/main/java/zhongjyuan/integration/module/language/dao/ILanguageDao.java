package zhongjyuan.integration.module.language.dao;

import zhongjyuan.domain.ITableDao;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;

public interface ILanguageDao extends ITableDao<LanguageDO> {

}
