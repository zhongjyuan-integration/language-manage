package zhongjyuan.integration.module.language.dao.impl;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.google.inject.Inject;

import zhongjyuan.domain.Constant;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.query.PageQuery;
import zhongjyuan.domain.query.SearchQuery;
import zhongjyuan.domain.query.SortQuery;
import zhongjyuan.easydb.IDBFactory;
import zhongjyuan.easydb.IPreparedStatementReadExecutor;
import zhongjyuan.easydb.IWriteExecutor;
import zhongjyuan.integration.annotation.PlatDB;
import zhongjyuan.integration.domain.AbstractTableDao;
import zhongjyuan.integration.module.language.dao.ILanguageDao;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;

public class LanguageDaoImpl extends AbstractTableDao<LanguageDO> implements ILanguageDao {

	@Inject
	private LanguageDaoImpl(@PlatDB IDBFactory dbFactory) {
		super(dbFactory, "plat_language");

		INSERT = MessageFormat.format("INSERT INTO `{0}` (`key`, `name`, `value`, `locale`, `package_name`, `package_version`, `description`, `is_enabled`, `is_deleted`, `row_index`, `creator_id`, `creator_name`, `create_time`, `updator_id`, `updator_name`, `update_time`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", TABLE_NAME);
		UPDATE = MessageFormat.format("UPDATE `{0}` SET `key` = ?, `name` = ?, `value` = ?, `locale` = ?, `package_name` = ?, `package_version` = ?, `description` = ?, `is_enabled` = ?, `is_deleted` = ?, `row_index` = ?, `creator_id` = ?, `creator_name` = ?, `create_time` = ?, `updator_id` = ?, `updator_name` = ?, `update_time` = ? WHERE `{1}` = 1;", TABLE_NAME, Constant.ID_L);
	}

	@Override
	public boolean[] insert(List<LanguageDO> entities) {
		boolean[] values = new boolean[entities.size()];

		if (null != dbFactory) {
			try {

				dbFactory.call(INSERT, new IWriteExecutor() {

					@Override
					public void handlePreparedStatement(PreparedStatement paramPreparedStatement) throws SQLException {
						for (int i = 0; i < entities.size(); i++) {
							paramPreparedStatement.setString(1, entities.get(i).getKey());
							paramPreparedStatement.setString(2, entities.get(i).getName());
							paramPreparedStatement.setString(3, entities.get(i).getValue());
							paramPreparedStatement.setString(4, entities.get(i).getLocale());
							paramPreparedStatement.setString(5, entities.get(i).getPackageName());
							paramPreparedStatement.setString(6, entities.get(i).getPackageVersion());
							paramPreparedStatement.setString(7, entities.get(i).getDescription());
							paramPreparedStatement.setBoolean(8, entities.get(i).getIsEnabled());
							paramPreparedStatement.setBoolean(9, entities.get(i).getIsDeleted());
							paramPreparedStatement.setLong(10, entities.get(i).getRowIndex());
							paramPreparedStatement.setLong(11, entities.get(i).getCreatorId());
							paramPreparedStatement.setString(12, entities.get(i).getCreatorName());
							paramPreparedStatement.setTimestamp(13, Timestamp.valueOf(entities.get(i).getCreateTime()));
							paramPreparedStatement.setLong(14, entities.get(i).getUpdatorId());
							paramPreparedStatement.setString(15, entities.get(i).getUpdatorName());
							paramPreparedStatement.setTimestamp(16, Timestamp.valueOf(entities.get(i).getUpdateTime()));

							paramPreparedStatement.addBatch();
						}

						int[] batchResult = paramPreparedStatement.executeBatch();

						for (int i = 0; i < batchResult.length; i++) {
							values[i] = batchResult[i] > 0;
						}
					}
				});
			} catch (final SQLException e) {
				LOGGER.error("DAO-ERROR caught at insert from {}：{}.", TABLE_NAME, e.getMessage());
				throw new BusinessException("dao caught at insert.");
			}
		}

		return values;
	}

	@Override
	public boolean[] update(List<LanguageDO> entities) {
		boolean[] values = new boolean[entities.size()];

		if (null != dbFactory) {
			try {

				dbFactory.call(UPDATE, new IWriteExecutor() {

					@Override
					public void handlePreparedStatement(PreparedStatement paramPreparedStatement) throws SQLException {
						for (int i = 0; i < entities.size(); i++) {
							paramPreparedStatement.setString(1, entities.get(i).getKey());
							paramPreparedStatement.setString(2, entities.get(i).getName());
							paramPreparedStatement.setString(3, entities.get(i).getValue());
							paramPreparedStatement.setString(4, entities.get(i).getLocale());
							paramPreparedStatement.setString(5, entities.get(i).getPackageName());
							paramPreparedStatement.setString(6, entities.get(i).getPackageVersion());
							paramPreparedStatement.setString(7, entities.get(i).getDescription());
							paramPreparedStatement.setBoolean(8, entities.get(i).getIsEnabled());
							paramPreparedStatement.setBoolean(9, entities.get(i).getIsDeleted());
							paramPreparedStatement.setLong(10, entities.get(i).getRowIndex());
							paramPreparedStatement.setLong(11, entities.get(i).getCreatorId());
							paramPreparedStatement.setString(12, entities.get(i).getCreatorName());
							paramPreparedStatement.setTimestamp(13, Timestamp.valueOf(entities.get(i).getCreateTime()));
							paramPreparedStatement.setLong(14, entities.get(i).getUpdatorId());
							paramPreparedStatement.setString(15, entities.get(i).getUpdatorName());
							paramPreparedStatement.setTimestamp(16, Timestamp.valueOf(entities.get(i).getUpdateTime()));

							paramPreparedStatement.setLong(17, entities.get(i).getId());

							paramPreparedStatement.addBatch();
						}

						int[] batchResult = paramPreparedStatement.executeBatch();

						for (int i = 0; i < batchResult.length; i++) {
							values[i] = batchResult[i] > 0;
						}
					}
				});
			} catch (final SQLException e) {
				LOGGER.error("DAO-ERROR caught at update from {}：{}.", TABLE_NAME, e.getMessage());
				throw new BusinessException("dao caught at update.");
			}
		}

		return values;
	}

	@Override
	@SuppressWarnings("unused")
	public List<LanguageDO> selectList(Collection<Serializable> ids, SortQuery query) {
		List<LanguageDO> entities = new ArrayList<>();

		StringBuilder params = new StringBuilder();
		for (Serializable id : ids) {
			params.append("?, ");
		}
		params.deleteCharAt(params.length() - 1);

		if (null != dbFactory) {
			try {

				dbFactory.call(MessageFormat.format("{0} ({1})", SELECT_BY_IDS, params.toString()), new IPreparedStatementReadExecutor() {

					@Override
					public void setParams(PreparedStatement preparedStatement) throws SQLException {
						int parameterIndex = 1;
						for (Serializable id : ids) {
							preparedStatement.setLong(parameterIndex++, (Long) id);
						}
					}

					@Override
					public void handleResult(ResultSet resultSet) throws SQLException {
						while (resultSet.next()) {
							LanguageDO entity = new LanguageDO();

							entity.setId(resultSet.getLong("id"));
							entity.setKey(resultSet.getString("key"));
							entity.setName(resultSet.getString("name"));
							entity.setValue(resultSet.getString("value"));
							entity.setLocale(resultSet.getString("locale"));
							entity.setPackageName(resultSet.getString("package_name"));
							entity.setPackageVersion(resultSet.getString("package_version"));
							entity.setDescription(resultSet.getString("description"));
							entity.setIsEnabled(resultSet.getBoolean("is_enabled"));
							entity.setIsDeleted(resultSet.getBoolean("is_deleted"));
							entity.setRowIndex(resultSet.getLong("row_index"));
							entity.setCreatorId(resultSet.getLong("creator_id"));
							entity.setCreatorName(resultSet.getString("creator_name"));
							entity.setCreateTime(resultSet.getTimestamp("create_time").toLocalDateTime());
							entity.setUpdatorId(resultSet.getLong("updator_id"));
							entity.setUpdatorName(resultSet.getString("updator_name"));
							entity.setUpdateTime(resultSet.getTimestamp("update_time").toLocalDateTime());

							entities.add(entity);
						}
					}
				});
			} catch (final SQLException e) {
				LOGGER.error("DAO-ERROR caught at selectList from {}：{}.", TABLE_NAME, e.getMessage());
				throw new BusinessException("dao caught at list.");
			}
		}

		return entities;
	}

	@Override
	public List<LanguageDO> selectPage(LanguageDO entity, PageQuery query) {
		List<LanguageDO> languages = new ArrayList<LanguageDO>();

		String condition = this.wrapCondition(entity, query);

		if (null != dbFactory) {
			try {

				dbFactory.call(SELECT, new IPreparedStatementReadExecutor() {

					@Override
					public void setParams(PreparedStatement preparedStatement) throws SQLException {
						preparedStatement.setString(1, condition);
					}

					@Override
					public void handleResult(final ResultSet resultSet) throws SQLException {

						while (resultSet.next()) {
							LanguageDO language = new LanguageDO();

							language.setId(resultSet.getLong("id"));
							language.setKey(resultSet.getString("key"));
							language.setName(resultSet.getString("name"));
							language.setValue(resultSet.getString("value"));
							language.setLocale(resultSet.getString("locale"));
							language.setPackageName(resultSet.getString("package_name"));
							language.setPackageVersion(resultSet.getString("package_version"));
							language.setDescription(resultSet.getString("description"));
							language.setIsEnabled(resultSet.getBoolean("is_enabled"));
							language.setIsDeleted(resultSet.getBoolean("is_deleted"));
							language.setRowIndex(resultSet.getLong("row_index"));
							language.setCreatorId(resultSet.getLong("creator_id"));
							language.setCreatorName(resultSet.getString("creator_name"));
							language.setCreateTime(resultSet.getTimestamp("create_time").toLocalDateTime());
							language.setUpdatorId(resultSet.getLong("updator_id"));
							language.setUpdatorName(resultSet.getString("updator_name"));
							language.setUpdateTime(resultSet.getTimestamp("update_time").toLocalDateTime());

							languages.add(language);
						}
					}
				});
			} catch (final SQLException e) {
				LOGGER.error("DAO-ERROR caught at selectPage from {}：{}.", TABLE_NAME, e.getMessage());
				throw new BusinessException("dao caught at page.");
			}
		}

		return languages;
	}

	protected <Q extends SearchQuery> String wrapCondition(LanguageDO entity, Q query) {

		StringBuilder condition = new StringBuilder();

		condition.append(" AND ");
		if (entity != null) {
			if (entity.getId() != null) {
				condition.append("`").append(Constant.ID_L).append("`").append(" = ").append(entity.getId()).append(" AND ");
			}

			if (entity.getKey() != null) {
				condition.append("`").append(Constant.KEY_L).append("`").append(" = ").append("'" + entity.getKey() + "'").append(" AND ");
			}

			if (entity.getName() != null) {
				condition.append("`").append(Constant.NAME_L).append("`").append(" = ").append("'" + entity.getName() + "'").append(" AND ");
			}

			if (entity.getValue() != null) {
				condition.append("`").append(Constant.VALUE_L).append("`").append(" = ").append("'" + entity.getValue() + "'").append(" AND ");
			}

			if (entity.getLocale() != null) {
				condition.append("`").append("locale").append("`").append(" = ").append("'" + entity.getLocale() + "'").append(" AND ");
			}

			if (entity.getPackageName() != null) {
				condition.append("`").append("package_name").append("`").append(" = ").append("'" + entity.getPackageName() + "'").append(" AND ");
			}

			if (entity.getPackageVersion() != null) {
				condition.append("`").append("package_version").append("`").append(" = ").append("'" + entity.getPackageVersion() + "'").append(" AND ");
			}

			if (entity.getDescription() != null) {
				condition.append("`").append(Constant.DESCIPTION_L).append("`").append(" = ").append("'" + entity.getDescription() + "'").append(" AND ");
			}

			if (entity.getIsEnabled() != null) {
				condition.append("`").append(Constant.IS_ENABLED_HUMP_L_A).append("`").append(" = ").append(entity.getIsEnabled()).append(" AND ");
			}

			if (entity.getIsDeleted() != null) {
				condition.append("`").append(Constant.IS_DELETED_HUMP_L_A).append("`").append(" = ").append(entity.getIsDeleted()).append(" AND ");
			}

			if (entity.getRowIndex() != null) {
				condition.append("`").append(Constant.ROW_INDEX_HUMP_L_A).append("`").append(" = ").append(entity.getRowIndex()).append(" AND ");
			}

			if (entity.getCreatorId() != null) {
				condition.append("`").append(Constant.CREATOR_ID_HUMP_L_A).append("`").append(" = ").append(entity.getCreatorId()).append(" AND ");
			}

			if (entity.getCreatorName() != null) {
				condition.append("`").append(Constant.CREATOR_NAME_HUMP_L_A).append("`").append(" = ").append("'" + entity.getCreatorName() + "'").append(" AND ");
			}

			if (entity.getCreateTime() != null) {
				condition.append("`").append(Constant.CREATE_TIME_HUMP_L_A).append("`").append(" = ").append("'" + entity.getCreateTime() + "'").append(" AND ");
			}

			if (entity.getUpdatorId() != null) {
				condition.append("`").append(Constant.UPDATOR_ID_HUMP_L_A).append("`").append(" = ").append(entity.getUpdatorId()).append(" AND ");
			}

			if (entity.getUpdatorName() != null) {
				condition.append("`").append(Constant.UPDATOR_NAME_HUMP_L_A).append("`").append(" = ").append("'" + entity.getUpdatorName() + "'").append(" AND ");
			}

			if (entity.getUpdateTime() != null) {
				condition.append("`").append(Constant.UPDATE_TIME_HUMP_L_A).append("`").append(" = ").append("'" + entity.getUpdateTime() + "'").append(" AND ");
			}
		}

		// 去除末尾的"AND"
		if (condition.length() > 0) {
			condition.setLength(condition.length() - 5); // 去除最后的 " AND "
		} else {
			condition.append(" 1 = 1 ");
		}

		String limitClause = "";
		String searchClause = "";
		String orderByClause = "";
		if (query != null) {
			String searchValue = Optional.ofNullable(query).map(SearchQuery::getSearchValue).orElse("");
			searchClause = Optional.ofNullable(searchValue).filter(value -> !value.isEmpty()).map(value -> String.format(" AND (key like '%%%s%%' OR name like '%%%s%%' OR value like '%%%s%%' OR package_name like '%%%s%%' OR description like '%%%s%%')", value, value, value)).orElse("");

			orderByClause = Optional.ofNullable(query).filter(q -> q instanceof SortQuery).map(q -> (SortQuery) q).map(sortQuery -> {
				String sortName = Optional.ofNullable(sortQuery.getSortName()).orElse(Constant.ROW_INDEX_L_A);
				String sortType = Optional.ofNullable(sortQuery.getSortType()).orElse(Constant.ASC_SORT_TYPE);

				switch (sortType.toLowerCase()) {
					case Constant.ASC_SORT_TYPE_INT:
					case Constant.ASCENDING_SORT_TYPE:
						sortType = Constant.ASC_SORT_TYPE;
						break;
					case Constant.DESC_SORT_TYPE_INT:
					case Constant.DESCENDING_SORT_TYPE:
						sortType = Constant.DESC_SORT_TYPE;
						break;
					default:
						sortType = Constant.ASC_SORT_TYPE;
						break;
				}

				return String.format(" ORDER BY %s %s", sortName, sortType);
			}).orElse("");

			limitClause = Optional.ofNullable(query).filter(q -> q instanceof PageQuery).map(q -> (PageQuery) q).map(pageQuery -> {
				Long pageIndex = Optional.ofNullable(pageQuery.getPageIndex()).orElse(1L);
				Long pageSize = Optional.ofNullable(pageQuery.getPageSize()).orElse(20L);

				Long offset = (pageIndex - 1L) * pageSize;
				return String.format(" LIMIT %d,%d", offset, pageSize);
			}).orElse("");
		}

		return condition.toString() + searchClause + orderByClause + limitClause;
	}
}
