package zhongjyuan.integration.module.language.message;

import java.io.Serializable;
import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.query.BatchQuery;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageBriefVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x7E,
	desc = "语言JSON双工消息批量查询简要数据封包")
@UnDispatchable
public class LanguageJsonDuplexBatchSelectBriefMessage extends AbstractJsonDuplexMessage {

	@Inject
	ILanguageService service;

	BatchQuery<Serializable> request;

	ResponseResult<List<LanguageBriefVO>> response = new ResponseResult<List<LanguageBriefVO>>(false, ResponseCode.FAILURE);

	List<LanguageBriefVO> vos;

	List<LanguageDO> entities;

	@Override
	@SuppressWarnings("unchecked")
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(BatchQuery.class);
		if (request != null) {

			entities = service.selectList(request.getAssemble());

			vos = Converter.entityToBriefVos(entities, LanguageBriefVO::new);
		}

		if (vos != null) {
			response = new ResponseResult<List<LanguageBriefVO>>(true, vos);
		}

		setData(response);
	}
}
