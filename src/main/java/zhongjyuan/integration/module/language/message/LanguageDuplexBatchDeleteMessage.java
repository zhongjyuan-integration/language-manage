package zhongjyuan.integration.module.language.message;

import java.io.Serializable;
import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.query.BatchQuery;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x79,
	desc = "语言双工消息批量删除数据封包")
@UnDispatchable
public class LanguageDuplexBatchDeleteMessage extends AbstractDuplexMessage {

	@Inject
	ILanguageService service;

	BatchQuery<Serializable> request;

	List<LanguageVO> vos;

	List<LanguageDO> entities;

	@Override
	protected void execute(MessageReceipt receipt) throws Exception {
		if (request != null) {
			entities = service.delete(request.getAssemble());

			vos = Converter.entityToVos(entities, LanguageVO::new);
		}

		receipt.getSession().write(this);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		if (vos != null) {
			writer.writeBoolean(true);
		} else {
			writer.writeBoolean(false);
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		if (reader.hasRemaining()) {
			request = new BatchQuery<Serializable>();

			int length = reader.readInt();
			for (int i = 0; i < length; i++) {
				request.put(reader.readLong());
			}
		}
	}

}
