package zhongjyuan.integration.module.language.message;

import com.google.inject.Inject;

import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.module.language.model.query.LanguageLoadQuery;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x7F,
	desc = "语言双工消息加载数据封包")
@UnDispatchable
public class LanguageJsonDuplexLoadMessage extends AbstractJsonDuplexMessage {

	@Inject
	ILanguageService service;

	LanguageLoadQuery request;

	ResponseResult<Object> response = new ResponseResult<Object>(false, ResponseCode.FAILURE);

	Object result;

	@Override
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(LanguageLoadQuery.class);
		if (request != null) {
			if (request.isAll()) {
				result = service.getLanguages();
			} else if (request.isJustForLocale()) {
				result = service.getLanguages(request.getLocale());
			} else if (request.isJustForBundle()) {
				result = service.getLanguages(request.getLocale(), request.getBundle());
			} else if (request.isJustForKey()) {
				result = service.getLanguage(request.getLocale(), request.getBundle(), request.getKey(), request.getVersion());
			}
		}

		if (result != null) {
			response = new ResponseResult<Object>(true, result);
		}

		setData(response);
	}
}
