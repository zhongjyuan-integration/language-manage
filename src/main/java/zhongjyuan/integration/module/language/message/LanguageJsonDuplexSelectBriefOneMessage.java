package zhongjyuan.integration.module.language.message;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageBriefVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x74,
	desc = "语言JSON双工消息查询简要数据封包")
@UnDispatchable
public class LanguageJsonDuplexSelectBriefOneMessage extends AbstractJsonDuplexMessage {

	@Inject
	ILanguageService service;

	Long request;

	ResponseResult<LanguageBriefVO> response = new ResponseResult<LanguageBriefVO>(false, ResponseCode.FAILURE);

	LanguageBriefVO vo;

	LanguageDO entity;

	@Override
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(Long.class);
		if (request != null) {

			entity = service.select(request);

			vo = Converter.entityToBriefVo(entity, LanguageBriefVO::new);
		}

		if (vo != null) {
			response = new ResponseResult<LanguageBriefVO>(true, vo);
		}

		setData(response);
	}
}
