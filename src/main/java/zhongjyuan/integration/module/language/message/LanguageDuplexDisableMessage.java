package zhongjyuan.integration.module.language.message;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x6B,
	desc = "语言双工消息禁用数据封包")
@UnDispatchable
public class LanguageDuplexDisableMessage extends AbstractDuplexMessage {

	@Inject
	ILanguageService service;

	Long request;

	LanguageVO vo;

	LanguageDO entity;

	@Override
	protected void execute(MessageReceipt receipt) throws Exception {
		if (request != null) {

			entity = service.select(request);
			entity.setIsEnabled(false);

			entity = service.update(entity);

			vo = Converter.entityToVo(entity, LanguageVO::new);
		}

		receipt.getSession().write(this);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		if (vo != null) {
			writer.writeBoolean(true);
		} else {
			writer.writeBoolean(false);
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		if (reader.hasRemaining()) {
			request = reader.readLong();
		}
	}

}
