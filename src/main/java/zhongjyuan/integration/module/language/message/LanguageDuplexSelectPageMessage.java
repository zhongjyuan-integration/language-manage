package zhongjyuan.integration.module.language.message;

import java.time.ZoneOffset;
import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.response.PageResult;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguagePageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguagePageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractPageDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x77,
	desc = "语言双工消息查询分页数据封包")
@UnDispatchable
public class LanguageDuplexSelectPageMessage extends AbstractPageDuplexMessage {

	@Inject
	ILanguageService service;

	LanguagePageQuery request;

	List<LanguagePageVO> vos;

	List<LanguageDO> entities;

	PageResult<LanguagePageVO> pageResult;

	@Override
	protected void execute(MessageReceipt receipt) throws Exception {
		if (request != null) {
			pageResult = service.selectPage(request);
		}

		receipt.getSession().write(this);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		if (vos != null) {
			writer.writeBoolean(true);

			int size = vos.size();
			writer.writeInt(size);

			for (int i = 0; i < size; i++) {
				LanguagePageVO vo = vos.get(i);

				writer.writeLong(vo.getId());

				writer.writePrefixString(vo.getKey());
				writer.writePrefixString(vo.getName());
				writer.writePrefixString(vo.getValue());
				writer.writePrefixString(vo.getLocale());
				writer.writePrefixString(vo.getPackageName());
				writer.writePrefixString(vo.getPackageVersion());

				writer.writeLong(vo.getRowIndex());

				writer.writeBoolean(vo.getIsEnabled());
				writer.writePrefixString(vo.getDescription());

				writer.writeLong(vo.getCreatorId());
				writer.writePrefixString(vo.getCreatorName());
				writer.writeLong(vo.getCreateTime().toInstant(ZoneOffset.UTC).toEpochMilli());

				writer.writeLong(vo.getUpdatorId());
				writer.writePrefixString(vo.getUpdatorName());
				writer.writeLong(vo.getUpdateTime().toInstant(ZoneOffset.UTC).toEpochMilli());
			}
		} else {
			writer.writeBoolean(false);
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		if (reader.hasRemaining()) {
			request = new LanguagePageQuery();

			request.setLocale(reader.readPrefixString());
			request.setPackageName(reader.readPrefixString());
			request.setPackageVersion(reader.readPrefixString());

			request.setPageIndex(reader.readLong());
			request.setPageSize(reader.readLong());

			request.setSortName(reader.readPrefixString());
			request.setSortType(reader.readPrefixString());

			request.setSearchValue(reader.readPrefixString());
		}
	}

	@Override
	public PageResult<?> getResult() {
		return pageResult;
	}

}
