package zhongjyuan.integration.module.language.message;

import java.io.Serializable;
import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.query.BatchQuery;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x7C,
	desc = "语言JSON双工消息批量查询详情数据封包")
@UnDispatchable
public class LanguageJsonDuplexBatchSelectMessage extends AbstractJsonDuplexMessage {

	@Inject
	ILanguageService service;

	BatchQuery<Serializable> request;

	ResponseResult<List<LanguageVO>> response = new ResponseResult<List<LanguageVO>>(false, ResponseCode.FAILURE);

	List<LanguageVO> vos;

	List<LanguageDO> entities;

	@Override
	@SuppressWarnings("unchecked")
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(BatchQuery.class);
		if (request != null) {

			entities = service.selectList(request.getAssemble());

			vos = Converter.entityToVos(entities, LanguageVO::new);
		}

		if (vos != null) {
			response = new ResponseResult<List<LanguageVO>>(true, vos);
		}

		setData(response);
	}
}
