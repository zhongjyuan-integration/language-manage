package zhongjyuan.integration.module.language.message;

/**
 * 101 - 200
 * 
 * LanguageDuplexCreateMessage => 0x65(101)
 * LanguageJsonDuplexCreateMessage => 0x66(102)
 * 
 * LanguageDuplexUpdateMessage => 0x67(103)
 * LanguageJsonDuplexUpdateMessage => 0x68(104)
 * 
 * LanguageDuplexDeleteMessage => 0x69(105)
 * LanguageJsonDuplexDeleteMessage => 0x6A(106)
 * 
 * LanguageDuplexDisableMessage => 0x6B(107)
 * LanguageJsonDuplexDisableMessage => 0x6C(108)
 * 
 * LanguageDuplexEnableMessage => 0x6D(109)
 * LanguageJsonDuplexEnableMessage => 0x6E(110)
 * 
 * LanguageDuplexSelectOneMessage => 0x6F(111)
 * LanguageJsonDuplexSelectOneMessage => 0x70(112)
 * 
 * LanguageDuplexSelectListMessage => 0x71(113)
 * LanguageJsonDuplexSelectListMessage => 0x72(114)
 * 
 * LanguageDuplexSelectBriefOneMessage => 0x73(115)
 * LanguageJsonDuplexSelectBriefOneMessage => 0x74(116)
 * 
 * LanguageDuplexSelectBriefListMessage => 0x75(117)
 * LanguageJsonDuplexSelectBriefListMessage => 0x76(118)
 * 
 * LanguageDuplexSelectPageMessage => 0x77(119)
 * LanguageJsonDuplexSelectPageMessage => 0x78(120)
 * 
 * LanguageDuplexBatchDeleteMessage => 0x79(121)
 * LanguageJsonDuplexBatchDeleteMessage => 0x7A(122)
 * 
 * LanguageDuplexBatchSelectMessage => 0x7B(123)
 * LanguageJsonDuplexBatchSelectMessage => 0x7C(124)
 * 
 * LanguageDuplexBatchSelectBriefMessage => 0x7D(125)
 * LanguageJsonDuplexBatchSelectBriefMessage => 0x7E(126)
 * 
 * LanguageJsonDuplexLoadMessage => 0x7F(127)
 */