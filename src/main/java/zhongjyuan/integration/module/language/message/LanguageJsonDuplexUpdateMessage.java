package zhongjyuan.integration.module.language.message;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.request.LanguageUpdateVO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x68,
	desc = "语言JSON双工消息更新数据封包")
@UnDispatchable
public class LanguageJsonDuplexUpdateMessage extends AbstractJsonDuplexMessage {

	@Inject
	ILanguageService service;

	LanguageUpdateVO request;

	ResponseResult<LanguageVO> response = new ResponseResult<LanguageVO>(false, ResponseCode.FAILURE);

	LanguageVO vo;

	LanguageDO entity;

	@Override
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(LanguageUpdateVO.class);
		if (request != null) {
			entity = Converter.updateVoToEntity(request, LanguageDO::new);

			entity = service.update(entity);

			vo = Converter.entityToVo(entity, LanguageVO::new);
		}

		if (vo != null) {
			response = new ResponseResult<LanguageVO>(true, vo);
		}

		setData(response);
	}
}
