package zhongjyuan.integration.module.language.message;

import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguagePageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguagePageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.message.AbstractJsonPageDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;

@Token(
	id = 0x78,
	desc = "语言JSON双工消息查询分页数据封包")
@UnDispatchable
public class LanguageJsonDuplexSelectPageMessage extends AbstractJsonPageDuplexMessage {

	@Inject
	ILanguageService service;

	LanguagePageQuery request;

	ResponseResult<PageResult<LanguagePageVO>> response = new ResponseResult<PageResult<LanguagePageVO>>(false, ResponseCode.FAILURE);

	List<LanguagePageVO> vos;

	List<LanguageDO> entities;

	PageResult<LanguagePageVO> pageResult;

	@Override
	protected void handle(MessageReceipt receipt) throws Exception {
		request = getData(LanguagePageQuery.class);
		if (request != null) {
			pageResult = service.selectPage(request);
		}

		if (pageResult != null) {
			response = new ResponseResult<PageResult<LanguagePageVO>>(true, pageResult);
		}

		setData(response);
	}

	@Override
	public PageResult<?> getResult() {
		return pageResult;
	}
}
