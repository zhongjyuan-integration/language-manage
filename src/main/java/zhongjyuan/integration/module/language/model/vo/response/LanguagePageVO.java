package zhongjyuan.integration.module.language.model.vo.response;

import java.time.LocalDateTime;

public class LanguagePageVO extends LanguageBriefVO {

	private static final long serialVersionUID = 1L;

	private String packageName;

	private String packageVersion;

	private Long rowIndex;

	private Boolean isEnabled;

	private String description;

	private Long creatorId;

	private String creatorName;

	private LocalDateTime createTime;

	private Long updatorId;

	private String updatorName;

	private LocalDateTime updateTime;

	/**
	 * @title:  getPackageName
	 * @description: getPackageName
	 * @return: String
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @title:  setPackageName
	 * @description: setPackageName
	 * @param String
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @title:  getPackageVersion
	 * @description: getPackageVersion
	 * @return: String
	 */
	public String getPackageVersion() {
		return packageVersion;
	}

	/**
	 * @title:  setPackageVersion
	 * @description: setPackageVersion
	 * @param String
	 */
	public void setPackageVersion(String packageVersion) {
		this.packageVersion = packageVersion;
	}

	/**
	 * @title:  getRowIndex
	 * @description: getRowIndex
	 * @return: Long
	 */
	public Long getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title:  setRowIndex
	 * @description: setRowIndex
	 * @param Long
	 */
	public void setRowIndex(Long rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title:  getIsEnabled
	 * @description: getIsEnabled
	 * @return: Boolean
	 */
	public Boolean getIsEnabled() {
		return isEnabled;
	}

	/**
	 * @title:  setIsEnabled
	 * @description: setIsEnabled
	 * @param Boolean
	 */
	public void setIsEnabled(Boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	/**
	 * @title:  getDescription
	 * @description: getDescription
	 * @return: String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title:  setDescription
	 * @description: setDescription
	 * @param String
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @title:  getCreatorId
	 * @description: getCreatorId
	 * @return: Long
	 */
	public Long getCreatorId() {
		return creatorId;
	}

	/**
	 * @title:  setCreatorId
	 * @description: setCreatorId
	 * @param Long
	 */
	public void setCreatorId(Long creatorId) {
		this.creatorId = creatorId;
	}

	/**
	 * @title:  getCreatorName
	 * @description: getCreatorName
	 * @return: String
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @title:  setCreatorName
	 * @description: setCreatorName
	 * @param String
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @title:  getCreateTime
	 * @description: getCreateTime
	 * @return: LocalDateTime
	 */
	public LocalDateTime getCreateTime() {
		return createTime;
	}

	/**
	 * @title:  setCreateTime
	 * @description: setCreateTime
	 * @param LocalDateTime
	 */
	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	/**
	 * @title:  getUpdatorId
	 * @description: getUpdatorId
	 * @return: Long
	 */
	public Long getUpdatorId() {
		return updatorId;
	}

	/**
	 * @title:  setUpdatorId
	 * @description: setUpdatorId
	 * @param Long
	 */
	public void setUpdatorId(Long updatorId) {
		this.updatorId = updatorId;
	}

	/**
	 * @title:  getUpdatorName
	 * @description: getUpdatorName
	 * @return: String
	 */
	public String getUpdatorName() {
		return updatorName;
	}

	/**
	 * @title:  setUpdatorName
	 * @description: setUpdatorName
	 * @param String
	 */
	public void setUpdatorName(String updatorName) {
		this.updatorName = updatorName;
	}

	/**
	 * @title:  getUpdateTime
	 * @description: getUpdateTime
	 * @return: LocalDateTime
	 */
	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	/**
	 * @title:  setUpdateTime
	 * @description: setUpdateTime
	 * @param LocalDateTime
	 */
	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}
}
