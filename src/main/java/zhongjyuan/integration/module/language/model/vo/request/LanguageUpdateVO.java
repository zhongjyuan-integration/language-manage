package zhongjyuan.integration.module.language.model.vo.request;

public class LanguageUpdateVO extends LanguageCreateVO {

	private static final long serialVersionUID = 1L;

	private Long id;

	/**
	 * @title:  getId
	 * @description: getId
	 * @return: Long
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @title:  setId
	 * @description: setId
	 * @param Long
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
