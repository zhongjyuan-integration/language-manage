package zhongjyuan.integration.module.language.model.entity;

import zhongjyuan.domain.AbstractTableEntity;
import zhongjyuan.domain.ITableEntity;

public class LanguageDO extends AbstractTableEntity implements ITableEntity {

	private static final long serialVersionUID = 1L;

	private String key;

	private String name;

	private String value;

	private String locale;

	private String packageName;

	private String packageVersion;

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLocale() {
		return this.locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getPackageName() {
		return this.packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageVersion() {
		return this.packageVersion;
	}

	public void setPackageVersion(String packageVersion) {
		this.packageVersion = packageVersion;
	}

}
