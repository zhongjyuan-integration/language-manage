package zhongjyuan.integration.module.language.model.vo.response;

import zhongjyuan.domain.AbstractModel;

public class LanguageBriefVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String key;

	private String name;

	private String value;

	private String locale;

	/**
	 * @title:  getId
	 * @description: getId
	 * @return: Long
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @title:  setId
	 * @description: setId
	 * @param Long
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @title:  getKey
	 * @description: getKey
	 * @return: String
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title:  setKey
	 * @description: setKey
	 * @param String
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title:  getName
	 * @description: getName
	 * @return: String
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title:  setName
	 * @description: setName
	 * @param String
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title:  getValue
	 * @description: getValue
	 * @return: String
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @title:  setValue
	 * @description: setValue
	 * @param String
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @title:  getLocale
	 * @description: getLocale
	 * @return: String
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @title:  setLocale
	 * @description: setLocale
	 * @param String
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}
}
