package zhongjyuan.integration.module.language.model.query;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonAutoDetect(JsonMethod.FIELD)
@JsonIgnoreProperties(
	ignoreUnknown = true)
public class LanguageLoadQuery {

	@JsonProperty("locale")
	private String locale;

	@JsonProperty("bundle")
	private String bundle;

	@JsonProperty("version")
	private String version;

	@JsonProperty("key")
	private String key;

	public LanguageLoadQuery() {

	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isAll() {
		return StringUtils.isEmpty(locale) && StringUtils.isEmpty(bundle) && StringUtils.isEmpty(key);
	}

	public boolean isJustForLocale() {
		return !StringUtils.isEmpty(locale) && StringUtils.isEmpty(bundle) && StringUtils.isEmpty(key);
	}

	public boolean isJustForBundle() {
		return !StringUtils.isEmpty(locale) && !StringUtils.isEmpty(bundle) && StringUtils.isEmpty(key);
	}

	public boolean isJustForKey() {
		return !StringUtils.isEmpty(locale) && !StringUtils.isEmpty(bundle) && !StringUtils.isEmpty(key);
	}
}
