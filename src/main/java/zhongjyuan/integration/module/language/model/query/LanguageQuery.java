package zhongjyuan.integration.module.language.model.query;

import zhongjyuan.domain.query.SortQuery;

public class LanguageQuery extends SortQuery {

	private static final long serialVersionUID = 1L;

	private String locale;

	private String packageName;

	private String packageVersion;

	/**
	 * @title:  getLocale
	 * @description: getLocale
	 * @return: String
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @title:  setLocale
	 * @description: setLocale
	 * @param String
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @title:  getPackageName
	 * @description: getPackageName
	 * @return: String
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @title:  setPackageName
	 * @description: setPackageName
	 * @param String
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @title:  getPackageVersion
	 * @description: getPackageVersion
	 * @return: String
	 */
	public String getPackageVersion() {
		return packageVersion;
	}

	/**
	 * @title:  setPackageVersion
	 * @description: setPackageVersion
	 * @param String
	 */
	public void setPackageVersion(String packageVersion) {
		this.packageVersion = packageVersion;
	}
}
