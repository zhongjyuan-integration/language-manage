package zhongjyuan.integration.module.language.model.vo.response;

public class LanguageVO extends LanguagePageVO {

	private static final long serialVersionUID = 1L;

	private Boolean isDeleted;

	/**
	 * @title:  getIsDeleted
	 * @description: getIsDeleted
	 * @return: Boolean
	 */
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	/**
	 * @title:  setIsDeleted
	 * @description: setIsDeleted
	 * @param Boolean
	 */
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
