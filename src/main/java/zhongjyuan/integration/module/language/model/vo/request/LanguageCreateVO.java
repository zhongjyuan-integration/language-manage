package zhongjyuan.integration.module.language.model.vo.request;

import zhongjyuan.domain.AbstractModel;

public class LanguageCreateVO extends AbstractModel {

	private static final long serialVersionUID = 1L;

	private String key;

	private String name;

	private String value;

	private String locale;

	private String packageName;

	private String packageVersion;

	private Long rowIndex;

	private String description;

	/**
	 * @title:  getKey
	 * @description: getKey
	 * @return: String
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @title:  setKey
	 * @description: setKey
	 * @param String
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @title:  getName
	 * @description: getName
	 * @return: String
	 */
	public String getName() {
		return name;
	}

	/**
	 * @title:  setName
	 * @description: setName
	 * @param String
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @title:  getValue
	 * @description: getValue
	 * @return: String
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @title:  setValue
	 * @description: setValue
	 * @param String
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @title:  getLocale
	 * @description: getLocale
	 * @return: String
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @title:  setLocale
	 * @description: setLocale
	 * @param String
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @title:  getPackageName
	 * @description: getPackageName
	 * @return: String
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @title:  setPackageName
	 * @description: setPackageName
	 * @param String
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @title:  getPackageVersion
	 * @description: getPackageVersion
	 * @return: String
	 */
	public String getPackageVersion() {
		return packageVersion;
	}

	/**
	 * @title:  setPackageVersion
	 * @description: setPackageVersion
	 * @param String
	 */
	public void setPackageVersion(String packageVersion) {
		this.packageVersion = packageVersion;
	}

	/**
	 * @title:  getRowIndex
	 * @description: getRowIndex
	 * @return: Long
	 */
	public Long getRowIndex() {
		return rowIndex;
	}

	/**
	 * @title:  setRowIndex
	 * @description: setRowIndex
	 * @param Long
	 */
	public void setRowIndex(Long rowIndex) {
		this.rowIndex = rowIndex;
	}

	/**
	 * @title:  getDescription
	 * @description: getDescription
	 * @return: String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @title:  setDescription
	 * @description: setDescription
	 * @param String
	 */
	public void setDescription(String description) {
		this.description = description;
	}
}
