package zhongjyuan.integration.module.language.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Maps;
import com.google.inject.Inject;

import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.integration.domain.AbstractTableService;
import zhongjyuan.integration.module.language.dao.ILanguageDao;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguagePageQuery;
import zhongjyuan.integration.module.language.model.query.LanguageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguagePageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

/**
 * @className: LanguageServiceImpl
 * @description: 语言服务实现类.拓展{@link AbstractTableService}类. 实现{@link ILanguageService}接口
 * @author: zhongjyuan
 * @date: 2023年11月22日 下午6:16:01
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class LanguageServiceImpl extends AbstractTableService<ILanguageDao, LanguageDO, LanguagePageVO, LanguageQuery, LanguagePageQuery> implements ILanguageService {

	@Inject
	private LanguageServiceImpl() {
		super();
	}

	@Override
	protected void createCheck(LanguageDO entity) {

		if (entity == null) {
			throw new BusinessException("创建失败,入参[entity]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getKey())) {
			throw new BusinessException("创建失败,属性[key]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getValue())) {
			throw new BusinessException("创建失败,属性[value]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getLocale())) {
			throw new BusinessException("更新失败,属性[locale]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getPackageName())) {
			throw new BusinessException("创建失败,属性[packageName]不能为空.");
		}

		LanguageDO existQuery = new LanguageDO();
		existQuery.setKey(entity.getKey());
		existQuery.setLocale(entity.getLocale());
		existQuery.setPackageName(entity.getPackageName());
		existQuery.setPackageVersion(entity.getPackageVersion());

		if (dao.exists(existQuery)) {
			throw new BusinessException("创建失败,已存在相同有效数据.");
		}
	}

	@Override
	protected LanguageDO updateCheck(LanguageDO entity) {

		if (entity == null) {
			throw new BusinessException("更新失败,入参[entity]不能为空.");
		}

		if (entity.getId() == null) {
			throw new BusinessException("更新失败,属性[id]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getKey())) {
			throw new BusinessException("更新失败,属性[key]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getValue())) {
			throw new BusinessException("更新失败,属性[value]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getLocale())) {
			throw new BusinessException("更新失败,属性[locale]不能为空.");
		}

		if (StringUtils.isEmpty(entity.getPackageName())) {
			throw new BusinessException("更新失败,属性[packageName]不能为空.");
		}

		LanguageDO originEntity = dao.selectOne(entity.getId());
		if (originEntity == null) {
			throw new BusinessException("更新失败,当前数据不存在.");
		}

		return originEntity;
	}

	@Override
	protected void deleteCheck(LanguageDO originEntity) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void deleteCheck(List<LanguageDO> originEntities) {
		// TODO Auto-generated method stub

	}

	@Override
	public Map<String, Map<String, Map<String, Properties>>> getLanguages() {
		Map<String, Map<String, Map<String, Properties>>> localeLanguages = Maps.newHashMap();

		List<LanguageDO> entities = this.dao.selectList(new LanguageDO());

		for (LanguageDO entity : entities) {
			String key = entity.getKey();
			String value = entity.getValue();
			String locale = entity.getLocale();
			String packageName = entity.getPackageName();
			String packageVersion = entity.getPackageVersion();

			// 获取或创建对应的properties对象
			Map<String, Map<String, Properties>> localeBundleLanguages = localeLanguages.computeIfAbsent(locale, k -> Maps.newHashMap());
			Map<String, Properties> localeBundleVersionLanguages = localeBundleLanguages.computeIfAbsent(packageName, k -> Maps.newHashMap());
			Properties language = localeBundleVersionLanguages.computeIfAbsent(packageVersion, k -> new Properties());

			// 将键值对放入properties对象中
			language.put(key, value);
		}

		// 返回结果
		return localeLanguages;
	}

	@Override
	public Map<String, Map<String, Properties>> getLanguages(String locale) {
		if (StringUtils.isNotEmpty(locale)) {
			Map<String, Map<String, Map<String, Properties>>> localeLanguages = this.getLanguages();
			return localeLanguages.getOrDefault(locale, null);
		}
		return null;
	}

	@Override
	public Map<String, Properties> getLanguages(String locale, String bundle) {
		if (StringUtils.isNotEmpty(locale) && StringUtils.isNotEmpty(bundle)) {
			Map<String, Map<String, Properties>> localeBundleLanguages = this.getLanguages(locale);
			return localeBundleLanguages.getOrDefault(bundle, null);
		}
		return null;
	}

	@Override
	public Properties getLanguages(String locale, String bundle, String version) {
		if (StringUtils.isNotEmpty(locale) && StringUtils.isNotEmpty(bundle) && StringUtils.isNotEmpty(version)) {
			Map<String, Properties> localeBundleVersionLanguages = this.getLanguages(locale, bundle);
			if (localeBundleVersionLanguages != null) {
				if (version != null) {
					return localeBundleVersionLanguages.getOrDefault(version, null);
				} else {
					Properties languages = new Properties();
					Map<Object, Integer> latestVersions = Maps.newHashMap();

					for (String bundleVersion : localeBundleVersionLanguages.keySet()) {

						int currentVersion = Integer.parseInt(bundleVersion);
						Properties language = localeBundleVersionLanguages.get(bundleVersion);

						for (Map.Entry<Object, Object> entry : language.entrySet()) {
							Object key = entry.getKey();
							Object value = entry.getValue();

							if (languages.containsKey(key) && latestVersions.containsKey(key)) {
								int existingVersion = latestVersions.get(key);
								if (currentVersion > existingVersion) {
									languages.put(key, value);
									latestVersions.put(key, currentVersion);
								}
							} else {
								languages.put(key, value);
								latestVersions.put(key, currentVersion);
							}
						}
					}
					return languages;
				}
			}
		}
		return null;
	}

	@Override
	public String getLanguage(String locale, String bundle, String key, String version) {
		if (StringUtils.isNotEmpty(locale) && StringUtils.isNotEmpty(bundle) && StringUtils.isNotEmpty(key)) {
			Properties language = this.getLanguages(locale, bundle, version);
			return language != null ? language.getProperty(key) : null;
		}
		return null;
	}
}
