package zhongjyuan.integration.module.language.service;

import java.util.Map;
import java.util.Properties;

import zhongjyuan.domain.ITableService;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguagePageQuery;
import zhongjyuan.integration.module.language.model.query.LanguageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguagePageVO;

/**
 * @className: ILanguageService
 * @description: 语言服务接口.实现{@link ITableService}接口
 * @param <LanguageDO>     表示语言的数据对象
 * @param <LanguagePageVO> 表示语言页面的视图对象
 * @param <LanguageQuery>  用于搜索语言的查询对象
 * @param <LanguagePageQuery> 用于分页列表的语言查询对象
 * @author: zhongjyuan
 * @date: 2023年11月22日 下午6:13:32
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public interface ILanguageService extends ITableService<LanguageDO, LanguagePageVO, LanguageQuery, LanguagePageQuery> {

	/**
	 * 返回所有可用语言的映射表，按照区域设置、捆绑包和版本进行索引。
	 *
	 * @return 所有可用语言的映射表
	 */
	Map<String, Map<String, Map<String, Properties>>> getLanguages();

	/**
	 * 返回给定区域设置下所有可用语言的映射表，按照捆绑包和版本进行索引。
	 *
	 * @param locale 要检索语言的区域设置
	 * @return 给定区域设置下所有可用语言的映射表
	 */
	Map<String, Map<String, Properties>> getLanguages(String locale);

	/**
	 * 返回给定区域设置和捆绑包下所有可用语言的映射表，按照版本进行索引。
	 *
	 * @param locale 要检索语言的区域设置
	 * @param bundle 要检索语言的捆绑包
	 * @return 给定区域设置和捆绑包下所有可用语言的映射表
	 */
	Map<String, Properties> getLanguages(String locale, String bundle);

	/**
	 * 返回给定区域设置、捆绑包和版本的语言字符串的属性对象。
	 *
	 * @param locale  要检索语言的区域设置
	 * @param bundle  要检索语言的捆绑包
	 * @param version 要检索的语言版本
	 * @return 包含给定区域设置、捆绑包和版本的语言字符串的属性对象
	 */
	Properties getLanguages(String locale, String bundle, String version);

	/**
	 * 返回给定区域设置、捆绑包、版本和键的语言字符串的值。
	 *
	 * @param locale  要检索语言的区域设置
	 * @param bundle  要检索语言的捆绑包
	 * @param key     要检索的语言键
	 * @param version 要检索的语言版本
	 * @return 给定区域设置、捆绑包、版本和键的语言字符串的值
	 */
	String getLanguage(String locale, String bundle, String key, String version);
}
