package zhongjyuan.integration.module.language.controller;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.query.BatchQuery;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/batch/delete")
public class LanguageBatchDeleteHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	BatchQuery<Serializable> request;

	ResponseResult<List<LanguageVO>> response = new ResponseResult<List<LanguageVO>>(false, ResponseCode.FAILURE);

	List<LanguageVO> vos;

	List<LanguageDO> entities;

	@Override
	@SuppressWarnings("unchecked")
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {

			this.request = new BatchQuery<Serializable>();
			this.request.setAssemble(JsonJackUtil.toJavaObject(request.getParameter("assemble"), Collection.class));

			if (this.request != null) {
				entities = service.delete(this.request.getAssemble());

				vos = Converter.entityToVos(entities, LanguageVO::new);
			}

			if (vos != null) {
				this.response = new ResponseResult<List<LanguageVO>>(true, vos);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
