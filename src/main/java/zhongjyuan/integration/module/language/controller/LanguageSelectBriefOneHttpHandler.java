package zhongjyuan.integration.module.language.controller;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageBriefVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/brief/one")
public class LanguageSelectBriefOneHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	Long request;

	ResponseResult<LanguageBriefVO> response = new ResponseResult<LanguageBriefVO>(false, ResponseCode.FAILURE);

	LanguageBriefVO vo;

	LanguageDO entity;

	@Override
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {
			this.request = Long.valueOf(request.getParameter("id"));

			if (this.request != null) {

				entity = service.select(this.request);

				vo = Converter.entityToBriefVo(entity, LanguageBriefVO::new);
			}

			if (vo != null) {
				this.response = new ResponseResult<LanguageBriefVO>(true, vo);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
