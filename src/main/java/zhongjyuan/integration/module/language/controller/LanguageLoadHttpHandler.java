package zhongjyuan.integration.module.language.controller;

import com.google.inject.Inject;

import zhongjyuan.domain.ResponseCode;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.query.LanguageLoadQuery;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/load")
public class LanguageLoadHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	LanguageLoadQuery request;

	ResponseResult<Object> response = new ResponseResult<Object>(false, ResponseCode.FAILURE);

	Object result;

	@Override
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {
			this.request = new LanguageLoadQuery();
			this.request.setKey(request.getParameter("key"));
			this.request.setLocale(request.getParameter("locale"));
			this.request.setBundle(request.getParameter("bundle"));
			this.request.setVersion(request.getParameter("version"));

			if (this.request != null) {
				if (this.request.isAll()) {
					result = service.getLanguages();
				} else if (this.request.isJustForLocale()) {
					result = service.getLanguages(this.request.getLocale());
				} else if (this.request.isJustForBundle()) {
					result = service.getLanguages(this.request.getLocale(), this.request.getBundle());
				} else if (this.request.isJustForKey()) {
					result = service.getLanguage(this.request.getLocale(), this.request.getBundle(), this.request.getKey(), this.request.getVersion());
				}
			}

			if (result != null) {
				this.response = new ResponseResult<Object>(true, result);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
