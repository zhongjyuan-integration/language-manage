package zhongjyuan.integration.module.language.controller;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.vo.request.LanguageUpdateVO;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/update")
public class LanguageUpdateHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	LanguageUpdateVO request;

	ResponseResult<LanguageVO> response = new ResponseResult<LanguageVO>(false, ResponseCode.FAILURE);

	LanguageVO vo;

	LanguageDO entity;

	@Override
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {
			this.request = new LanguageUpdateVO();
			this.request.setId(Long.valueOf(request.getParameter("id")));
			this.request.setKey(request.getParameter("key"));
			this.request.setName(request.getParameter("name"));
			this.request.setValue(request.getParameter("value"));
			this.request.setLocale(request.getParameter("locale"));
			this.request.setPackageName(request.getParameter("packageName"));
			this.request.setPackageVersion(request.getParameter("packageVersion"));
			this.request.setRowIndex(Long.valueOf(request.getParameter("rowIndex")));
			this.request.setDescription(request.getParameter("description"));

			if (this.request != null) {
				entity = Converter.updateVoToEntity(this.request, LanguageDO::new);

				entity = service.update(entity);

				vo = Converter.entityToVo(entity, LanguageVO::new);
			}

			if (vo != null) {
				this.response = new ResponseResult<LanguageVO>(true, vo);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
