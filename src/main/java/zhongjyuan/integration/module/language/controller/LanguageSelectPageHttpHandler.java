package zhongjyuan.integration.module.language.controller;

import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.response.PageResult;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguagePageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguagePageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/page")
public class LanguageSelectPageHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	LanguagePageQuery request;

	ResponseResult<PageResult<LanguagePageVO>> response = new ResponseResult<PageResult<LanguagePageVO>>(false, ResponseCode.FAILURE);

	List<LanguagePageVO> vos;

	List<LanguageDO> entities;

	PageResult<LanguagePageVO> pageResult;

	@Override
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {
			this.request = new LanguagePageQuery();
			this.request.setSortName(request.getParameter("sortName"));
			this.request.setSortType(request.getParameter("sortType"));
			this.request.setSearchValue(request.getParameter("searchValue"));

			this.request.setLocale(request.getParameter("locale"));
			this.request.setPackageName(request.getParameter("packageName"));
			this.request.setPackageVersion(request.getParameter("packageVersion"));

			if (this.request != null) {
				pageResult = service.selectPage(this.request);
			}

			if (pageResult != null) {
				this.response = new ResponseResult<PageResult<LanguagePageVO>>(true, pageResult);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
