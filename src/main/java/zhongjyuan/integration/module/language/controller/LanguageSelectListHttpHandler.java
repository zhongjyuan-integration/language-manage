package zhongjyuan.integration.module.language.controller;

import java.util.List;

import com.google.inject.Inject;

import zhongjyuan.domain.Converter;
import zhongjyuan.domain.exception.BusinessException;
import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.domain.utils.format.JsonJackUtil;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.http.AbstractHttpHandler;
import zhongjyuan.integration.module.http.IHttpHandler;
import zhongjyuan.integration.module.http.annotation.RequestMapping;
import zhongjyuan.integration.module.http.model.HttpRequestMessage;
import zhongjyuan.integration.module.http.model.HttpResponseMessage;
import zhongjyuan.integration.module.language.model.entity.LanguageDO;
import zhongjyuan.integration.module.language.model.query.LanguageQuery;
import zhongjyuan.integration.module.language.model.vo.response.LanguageVO;
import zhongjyuan.integration.module.language.service.ILanguageService;

@RequestMapping("languages/list")
public class LanguageSelectListHttpHandler extends AbstractHttpHandler implements IHttpHandler {

	@Inject
	ILanguageService service;

	LanguageQuery request;

	ResponseResult<List<LanguageVO>> response = new ResponseResult<List<LanguageVO>>(false, ResponseCode.FAILURE);

	List<LanguageVO> vos;

	List<LanguageDO> entities;

	@Override
	public HttpResponseMessage handle(HttpRequestMessage request) throws Exception {
		HttpResponseMessage response = new HttpResponseMessage();
		try {
			this.request = new LanguageQuery();
			this.request.setLocale(request.getParameter("locale"));
			this.request.setPackageName(request.getParameter("packageName"));
			this.request.setPackageVersion(request.getParameter("packageVersion"));

			if (this.request != null) {

				entities = service.selectList(this.request);

				vos = Converter.entityToVos(entities, LanguageVO::new);
			}

			if (vos != null) {
				this.response = new ResponseResult<List<LanguageVO>>(true, vos);
			}

			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(this.response));
		} catch (BusinessException e) {
			response.setContentType("application/json; charset=UTF-8");
			response.appendBody(JsonJackUtil.toISONString(e.getResponseResult()));
		}

		return response;
	}

}
