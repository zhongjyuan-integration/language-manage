package zhongjyuan.integration.module.language;

import java.util.Arrays;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;

import zhongjyuan.integration.module.AbstractModuleService;
import zhongjyuan.integration.module.DefaultModuleListener;
import zhongjyuan.integration.module.IModuleInfo;
import zhongjyuan.integration.module.ModuleException;
import zhongjyuan.integration.module.ModuleManage;
import zhongjyuan.integration.module.hotswap.AbstractHotSwapModuleAdapter;
import zhongjyuan.integration.module.language.controller.LanguageBatchDeleteHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageBatchSelectBriefHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageBatchSelectHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageCreateHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageDeleteHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageDisableHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageEnableHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageLoadHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageSelectBriefListHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageSelectBriefOneHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageSelectListHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageSelectOneHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageSelectPageHttpHandler;
import zhongjyuan.integration.module.language.controller.LanguageUpdateHttpHandler;
import zhongjyuan.integration.module.language.dao.ILanguageDao;
import zhongjyuan.integration.module.language.dao.impl.LanguageDaoImpl;
import zhongjyuan.integration.module.language.service.ILanguageService;
import zhongjyuan.integration.module.language.service.impl.LanguageServiceImpl;

/**
 * @className: BootAdapter
 * @description: 启动适配器，用于加载和启动应用程序。继承自{@link AbstractHotSwapModuleAdapter} ，并实现了相应的方法，用于热插拔模块的适配。
 * @author: zhongjyuan
 * @date: 2023年11月2日 上午9:53:58
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class BootAdapter extends AbstractHotSwapModuleAdapter {

	@Inject
	private Injector injector;

	@Inject
	private ModuleManage moduleManage;

	public BootAdapter(IModuleInfo hotSwapModuleInfo) {
		super(hotSwapModuleInfo);
	}

	@Override
	protected Iterable<? extends Module> init() {
		return super.init();
	}

	@Override
	protected Iterable<? extends Module> configure() throws Exception {
		return Arrays.asList(new AbstractModuleService() {

			@Override
			protected void configurate() {
				bind(ILanguageDao.class).to(LanguageDaoImpl.class);
				bind(ILanguageService.class).to(LanguageServiceImpl.class);
			}

		});
	}

	@Override
	protected void setupExtension() throws Exception {
		super.setupExtension();

		if (null == moduleManage) {
			throw new ModuleException("null module was not allowed!");
		}

		moduleManage.addListener(new DefaultModuleListener() {

			@Override
			public void startNotify(ModuleManage moduleManage) {
				injector.getInstance(LanguageCreateHttpHandler.class).register();
				injector.getInstance(LanguageUpdateHttpHandler.class).register();
				injector.getInstance(LanguageDeleteHttpHandler.class).register();
				injector.getInstance(LanguageDisableHttpHandler.class).register();
				injector.getInstance(LanguageEnableHttpHandler.class).register();
				injector.getInstance(LanguageSelectOneHttpHandler.class).register();
				injector.getInstance(LanguageSelectListHttpHandler.class).register();
				injector.getInstance(LanguageSelectBriefOneHttpHandler.class).register();
				injector.getInstance(LanguageSelectBriefListHttpHandler.class).register();
				injector.getInstance(LanguageSelectPageHttpHandler.class).register();
				injector.getInstance(LanguageBatchDeleteHttpHandler.class).register();
				injector.getInstance(LanguageBatchSelectHttpHandler.class).register();
				injector.getInstance(LanguageBatchSelectBriefHttpHandler.class).register();
				injector.getInstance(LanguageLoadHttpHandler.class).register();
			}
		});
	}

	@Override
	protected void startup() throws Exception {
		super.startup();
	}

	@Override
	protected void shutdown() throws Exception {
		super.shutdown();

	}
}
